from django import forms
from .models import TodoList, TodoItem


class TodoListForm(forms.ModelForm):
    name = forms.CharField(max_length=100, label="Name")

    class Meta:
        model = TodoList
        fields = ["name"]


class TodoItemForm(forms.ModelForm):
    class Meta:
        model = TodoItem
        fields = ["task", "due_date", "is_completed", "list"]
